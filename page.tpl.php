<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >

<head>
   <title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
</head>
<body>

<div id="wrapper">

  <div id="header">
    <div id="logo">
      <h1><a href="<?php print url() ?>" title="<?php print($site_name) ?>"><?php print($site_name) ?></a></h1>
      <div class="slogan"><?php print($site_slogan) ?></div>
    </div>
  
    <div id="navigation">
      <?php if ($search_box): ?>
		    <?php print $search_box ?>
      <?php endif; ?>
      <?php if (isset($primary_links)) : ?>
        <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
      <?php endif; ?>
    </div>
  </div>
<!-- end #header -->

<div id="page">
	<div id="content">
    <div id="header-pic"></div>	
	    <div class="navigation"> <?php print $breadcrumb ?> </div>
    <?php if ($messages != ""): ?>
    	<div id="message"><?php print $messages ?></div>
    <?php endif; ?>
    <?php if ($mission != ""): ?>
    	<div id="mission"><?php print $mission ?></div>
    <?php endif; ?>
    <?php if ($title != ""): ?>
    	<h2 class="page-title"><?php print $title ?></h2>
    <?php endif; ?>
    <?php if ($tabs != ""): ?>
    	<?php print $tabs ?>
    <?php endif; ?>
    <?php if ($help != ""): ?>
    	<p id="help"><?php print $help ?></p>
    <?php endif; ?>
    <!-- start main content -->
    <?php print($content) ?>
    <!-- end main content -->
  </div>
			
	<!-- end #content -->

<div id="sidebar">
	<div id="sidebar-content">
	   <?php print $right; ?>
	</div>

</div>
<!-- end #sidebar -->


	<br style="clear: both;" />
</div>
</div>
<!-- end #page -->

<div id="footer">
   <?php print $$footer; ?>
   <p>
		Freeradicals is proudly powered by <a href="http://Drupal.org/">Drupal</a>

		&nbsp;&bull;&nbsp;
		Design by <a href="http://www.Neokrish.co.in/">Free Drupal Themes</a>
		&nbsp;&bull;&nbsp;
		<a href="#">Entries (RSS)</a>
		&nbsp;&bull;&nbsp;
		<a href="#">Comments (RSS)</a>
	</p>

   <?php print $footer_message; ?>
</div>
<!-- end #footer -->


</body>
</html>
